LICENSE = "GPLv3"
SECTION = "libs"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = "git://gitlab.com/baylibre-acme/libsigrok.git;protocol=http;branch=libsigrok-0.3.x-iio"
SRCREV = "085212c424fff9fdca3239fa97d11f9c6b54e6d2"

S = "${WORKDIR}/git"

DEPENDS = "libiio libusb1 libcheck glib-2.0 libzip"

inherit pkgconfig autotools

# Disable demo to only have iio active
EXTRA_OECONF = "--enable-demo=no"
